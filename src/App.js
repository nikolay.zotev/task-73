import { useEffect, useState } from "react";
import "./App.css";


function App() {
  const [count, setCount] = useState(1);

  useEffect(() => {
    document.title = `Count: ${count}`;
  }, [count]);

  return (
    <div className="App">
      <button
        className="button is-medium"
        onClick={() => {
          setCount((count) => count + 1);
        }}
      >
        Count ({count})
      </button>
    </div>
  );
}

export default App;
